## INTRODUCTION

The Link with description - Attributes module is a module that combines the
[Link Attributes widget](https://www.drupal.org/project/link_attributes) and
[Link with description](https://www.drupal.org/project/link_description)
modules.

The primary use case for this module is:

- If you want to add link attributes to your link that has a description.

## REQUIREMENTS

* [Link Attributes widget](https://www.drupal.org/project/link_attributes)
* [Link with description](https://www.drupal.org/project/link_description)

These are download via composer as dependencies.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
The module has no menu or modifiable settings. There is no configuration.

In order to use this functionality, follow the following steps:

* Enable the module like normal
* For other link_description fields, edit the widget using 'Manage form display'
  and select the 'Link description (with attributes)' widget

## MAINTAINERS

Current maintainers for Drupal 10:

- Fabian de Rijk (fabianderijk) - https://www.drupal.org/u/fabianderijk

