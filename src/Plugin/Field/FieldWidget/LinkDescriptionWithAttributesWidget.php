<?php

namespace Drupal\link_description_attributes\Plugin\Field\FieldWidget;

use Drupal\link_attributes\Plugin\Field\FieldWidget\LinkWithAttributesWidget;
use Drupal\link_description_attributes\LinkDescriptionWithAttributesWidgetTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'link_description_attributes' widget.
 *
 * @FieldWidget(
 *   id = "link_description_attributes",
 *   label = @Translation("Link description (with attributes)"),
 *   field_types = {
 *     "link_description"
 *   }
 * )
 */
class LinkDescriptionWithAttributesWidget extends LinkWithAttributesWidget {

  use LinkDescriptionWithAttributesWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.link_attributes')
    );
  }

}
