<?php

namespace Drupal\link_description_attributes;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a trait for link widgets with attributes and description.
 */
trait LinkDescriptionWithAttributesWidgetTrait {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['description'] = [
      '#type' => 'textarea',
      '#rows' => 3,
      '#title' => $this->t('Long description'),
      '#default_value' => $items[$delta]->description ?? NULL,
    ];

    return $element;
  }

}
